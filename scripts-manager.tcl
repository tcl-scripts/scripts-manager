##
# Scripts Manager
##
# Simplify your management of scripts
# in your eggdrop
##

namespace eval sm {

	variable sname "Script Manager"
	variable codename "script-manager"
	variable version "1.0"
	variable author "CrazyCat"
	variable scripturl ""

	variable lang french
	variable Terror "\00304Error\003"
	variable Twarning "\00307Warning\003"
	variable Tsuccess "\00309Success\003"

	# Initial checks
	# http package is mandatory
	if {[catch {package require http}]} { putloglev o * "${Terror} \002\[${::sm::sname}\]\002 http package not found, aborting..."; return; } else { putloglev o * "${Tsuccess} \002\[${::sm::sname}\]\002 http package loaded"}
	# tls package is mandatory
	if {[catch {package require tls}]} { putloglev o * "${Terror} \[${::sm::sname}\] tls package not found, aborting..."; return; } else { putloglev o * "${Tsuccess} \002\[${::sm::sname}\]\002 tls package loaded"}
	# json package is recommended, using hack if not exists
	if {[catch {package require json}]} {
		putloglev o * "${Twarning} \002\[${::sm::sname}\]\002 json package not found, using hack..."
		namespace eval json {
			proc json2dict {JSONtext} {
				string range [string trim [string trimleft [string map {\t {} \n {} \r {} , { } : { } \[ \{ \] \}} $JSONtext] {\uFEFF}]] 1 end-1
			}
		}
	} else { putloglev o * "${Tsuccess} \002\[${::sm::sname}\]\002 json package loaded"}
	# msgcat package is recommended, but loaded at the end of file
	
	# path for script datas
	# hardcoded to avoid troubles in updates
	# checks are also hardcoded, the proc will be load only if script runs
	variable dpath "[file dirname [file normalize [info script]]]/.smdatas/"
	if {![file exists $::sm::dpath]} { file mkdir $::sm::dpath; }
	if {![file isdirectory $::sm::dpath]} { putloglev o * "${Terror} \002\[${::sm::sname}\]\002 $::sm::dpath exists but is not a directory, aborting..."; return; }
	if {![file writable $::sm::dpath]} { putloglev o * "${Terror} \002\[${::sm::sname}\]\002 $::sm::dpath exists but is not writable, aborting..."; return; }
	
	bind dcc n list ::sm::getlist
	bind dcc n install ::sm::install
	
	# List scripts
	# -i => installed scripts
	# -a => available scripts
	proc getlist {handle idx text} {
		set act [join [lindex [split $text] 0]]
		putlog "**$act**"
		switch -exact -- $act {
			"-i" {
				putlog "List installed scripts (with updatables)"
			}
			"-a" {
				putlog "List available scripts"
				set res [::sm::repolist]
			}
			default {
				putlog "Use .list \[-i | -a\]"
			}
		}
	}
	
	proc repolist {{type "all"}} {
		# for test: hardcoded here
		# must use .smdatas/scripts-manager.json to get the infos
		set datas [::json::json2dict [::sm::httpget "https://repo.eggdrop.fr/stable.json"]]
		if {$type == "all"} {
			foreach cn [dict keys $datas] {
				putlog "\002$cn\002 v\002[dict get $datas $cn version]\002 : [dict get $datas $cn description]"
			}
		} else {
			putlog $type
		}
	}

	# Install scripts
	# -u => upgrade (alternative to .upgrade)
	# -p => permanent (modify the .conf)
	proc install {handle idx text} {
		set args [split $text]
		switch [join [lindex $args 0]] {
			"-u" { ::sm::upgrade $handle $idx [join [lindex $args 1]] }
			"-p" { putlog "Installing permanent [join [lindex $args 1]]" }
			default { 
				putlog "Installing temporaly [join [lindex $args 1]]"
			}
		}
		return
	}
	
	# Upgrade script
	proc upgrade {handle idx text} {
		putlog "Upgrading [join [lindex $args 1]]"
		return
	}
	
	
	proc init {} {
		variable sname
		variable version
		variable author
		putlog "${sname} V${version} by ${author} loaded" 
	}

	proc deinit {} {
		putlog "unloaded"
	}
	
	proc httpget {url} {
		# @TODO : manage errors and various things
		::http::register https 443 [list ::tls::socket -autoservername true]
		set token [::http::geturl $url]
		set datas [::http::data $token]
		::http::cleanup $token
		::http::unregister https
		return $datas
	}

	# Loading of translation files
	proc i18n {msgfile {lang {}} } {
		if {$lang == ""} {set lang [string range [::msgcat::mclocale] 0 1] }
		if { [catch {open $msgfile r} fmsg] } {
			putloglev o *  "${::sm::Twarning} \002\[$sm::sname\]\002 Could not open $msgfile for reading\n$fmsg"
		} else {
			putloglev o *  "${::sm::Tsuccess} \002\[$sm::sname\]\002 - Loading $lang file from $msgfile"
			while {[gets $fmsg line] >= 0} {
				lappend ll $line
			}
			close $fmsg
			::msgcat::mcmset $lang [join $ll]
			unset ll
		}
		::msgcat::mclocale $lang
	}

	# here is the load of msgcat or its hack
	if {![catch {package require msgcat}]} {
		putloglev o * "${Tsuccess} \[${::sm::sname}\] msgcat package loaded"
		::msgcat::mclocale en
		if { [::msgcat::mc GREGORIAN_CHANGE_DATE] == "GREGORIAN_CHANGE_DATE"} {
			::msgcat::mcset [::msgcat::mclocale] GREGORIAN_CHANGE_DATE 2299527
		}
		if {$::sm::lang != "" && [string tolower $::sm::lang] != "english"} {
			set lfile "${::sm::dpath}[string tolower $::sm::lang].msg"
			if {[file exists $lfile]} {
				::sm::i18n $lfile [::tcl::string::map {"english" "en" "french" "fr" "spanish" "sp" "german" "de" "portuguese" "pt"} [string tolower $::sm::lang]]
			} else {
				putloglev o * "${::sm::Twarning} \002\[$sm::sname\]\002 Could not find $lfile"
			}
		}
	} else {
		putloglev o * "${::sm::Twarning} \002\[$sm::sname\]\002 package \002msgcat\002 not found, using hack"
		# short hack if msgcat cannot be load
		namespace eval ::msgcat {
			proc mc {text {str ""} args} { return [format $text $str $args] }
		}
	}

	[namespace current]::init

}

