# Scripts Manager

Small tool to manage scripts in an egdrop

## Installation
* Copy scripts-manager.tcl into your scripts/ directory
* Add `source scripts/scripts-manager.tcl` in your .conf file
* Rehash the eggdrop

## Upgrade
Just do `.upgrade scripts-manager` from partyline (must be eggdrop owner)

## Usage
### List scripts
`.list [-i | -a] [name]`
### Install a script
`.install [-p | -u | -f] name`
* **-p** makes script installed permanently (edit .conf file)
* **-u** will update the mentionned script. If script is not installed, will just install it
* **-f** option will erase previous configuration of the script if it exists (do a fresh install)

* _Note 1_ : `.install -u name` is an alias to `.upgrade name`
* _Note 2_ : the install command will source the script unless it needs a configuration. If configuration is needed, the status will be _pending_ and you will have to use the `.complete name` command to load and run

### Upgrade scripts
`.upgrade [name]`

Will upgrade [name] if mentionned, else will upgrade all managed scripts

### Special commands
* `.complete [name]` will source the script and flag it as _installed_ if it was in _pending_ status
